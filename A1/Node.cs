using System;
using System.Collections.Generic;
using System.Linq;

namespace GKIA1
{
    class Node
    {

        public static int maxDepth = 0;
        public static int maxBranch = 0;

        protected int[] state = { 3, 3, 0, 0, 0, 0, 0 };
        
        protected List<Node> children;
        protected Node prev;

        public Node(int[] state)
        {
            children = new List<Node>();
            this.state = state;
        }
        public void ExpandTree(int depth)
        {
            maxDepth = Math.Max(maxDepth, depth);
            

            InBoat(false);
            InBoat(true);
            OutBoat(true);
            OutBoat(false);
            Travel();

            //Remove all children with a circle state
            children.RemoveAll(s => s.HasCircle(s.state));

            //Iterate through cycle-less children and expand search tree
            foreach (Node s in children)
            {

                s.ExpandTree(depth + 1);
            }

            maxBranch = Math.Max(maxBranch, children.Count);
        }

        protected bool HasCircle(int[] comp)
        {
            if(prev == null)
            {
                return false;
            }
            return comp.SequenceEqual(prev.state) ? true : prev.HasCircle(comp);
        }
        
        protected void AddChild(int[] copy)
        {
            Node n = new Node(copy);
            n.prev = this;
            children.Add(n);
        }
        #region Actions

        void InBoat(bool T)
        {
            if(BoatCount() >= 2)
            {
                return;
            }

            int[] copy = CopyState();

            if (!T)
            {
                if (BoatLeft() && copy[1] > 0)
                {
                    copy[6]++;
                    copy[1]--;
                    AddChild(copy);
                }
                else if(!BoatLeft() && copy[3] > 0)
                {
                    copy[6]++;
                    copy[3]--;
                    AddChild(copy);
                }
            }
            else
            {
                if (BoatLeft() && state[0] > 0 && state[1] < state[0])
                {
                    copy[5]++;
                    copy[0]--;
                    AddChild(copy);
                }
                else if(!BoatLeft() && state[2] > 0 && state[3] < state[2])
                {
                    copy[5]++;
                    copy[2]--;
                    AddChild(copy);
                }
            }
        }
        void OutBoat(bool T)
        {
            if(BoatCount() <= 0)
            {
                return;
            }

            int[] copy = CopyState();

            if (T && copy[5] > 0)
            {
                copy[5]--;
                copy[BoatLeft() ? 0 : 2]++;
                AddChild(copy);
            }
            else if(!T && copy[6] > 0)
            {
                if (BoatLeft() && (copy[0] > copy[1] || copy[0] == 0))
                {
                    copy[6]--;
                    copy[1]++;
                    AddChild(copy);
                }
                else if (!BoatLeft() && (copy[2] > copy[3] || copy[2] == 0))
                {
                    copy[6]--;
                    copy[3]++;
                    AddChild(copy);
                }
            }
        }
        void Travel()
        {
            if(BoatCount() <= 0)
            {
                return;
            }
            int[] copy = CopyState();
            copy[4] = BoatLeft() ? 1 : 0;
            AddChild(copy);
        }
        
        #endregion

        public int BoatCount()
        {
            return state[5] + state[6];
        }
        public bool BoatLeft()
        {
            return state[4] == 0;
        }
        int[] CopyState()
        {
            int[] result = new int[state.Length];
            Buffer.BlockCopy(state, 0, result, 0, state.Length * sizeof(int));
            return result;
        }
        /// <summary>
        /// Prints the graph
        /// </summary>
        /// <param name="depth">Recursive parameter to check tree depth. Use 0</param>
        /// <param name="maxPrintDepth">Depth at which print cancels recursive calls</param>
        public void Print(int depth, int maxPrintDepth)
        {
            if (depth > maxPrintDepth)
            {
                return;
            }
            Console.WriteLine((new String('\t', depth)) + "(" + string.Join(",", state) + ")");
            foreach(Node s in children)
            {
                s.Print(depth + 1, maxPrintDepth);
            }
        }
    }
}
