/**

USAGE EXAMPLE

**/

using System;

namespace GKIA1
{
    class Program
    {
        static void Main(string[] args)
        {
            NodeSecond root2 = new NodeSecond(new int[] { 3, 3, 0, 0, 0 });
            root2.ExpandTree(0);
            root2.Print(0, 4);
            Console.WriteLine(string.Format("done\nmaxDepth:\t{0}\nmaxbranch:\t{1}", NodeSecond.maxDepth, NodeSecond.maxBranch));
            
            Node root = new Node(new int[] { 3,3,0,0,0,0,0});
            root.ExpandTree(0);
            root.Print(0, 4);
            //root.Print(0);
            Console.WriteLine(string.Format("done\nmaxDepth:\t{0}\nmaxbranch:\t{1}", Node.maxDepth, Node.maxBranch));
        }
    }
}