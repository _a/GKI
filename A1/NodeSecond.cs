using System;
using System.Collections.Generic;
using System.Linq;

namespace GKIA1
{
    class NodeSecond
    {

        public static int maxDepth = 0;
        public static int maxBranch = 0;

        protected int[] state = { 3, 3, 0, 0, 0};

        protected List<NodeSecond> children;
        protected NodeSecond prev;

        public NodeSecond(int[] state)
        {
            children = new List<NodeSecond>();
            this.state = state;
        }
        public void ExpandTree(int depth)
        {
            maxDepth = Math.Max(maxDepth, depth);

            Transport(1, 0);
            Transport(0, 1);
            Transport(1, 1);
            Transport(2, 0);
            Transport(0, 2);
            

            //Remove all children with a circle state
            children.RemoveAll(s => s.HasCircle(s.state));

            //Iterate through cycle-less children and expand search tree
            foreach (NodeSecond s in children)
            {
                s.ExpandTree(depth + 1);
            }

            maxBranch = Math.Max(maxBranch, children.Count);
        }

        protected bool HasCircle(int[] comp)
        {
            if (prev == null)
            {
                return false;
            }
            return comp.SequenceEqual(prev.state) ? true : prev.HasCircle(comp);
        }

        protected void AddChild(int[] copy)
        {
            NodeSecond n = new NodeSecond(copy);
            n.prev = this;
            children.Add(n);
        }
        #region Actions

        void Transport(int T, int R)
        {
            if(T + R < 1 && T + R > 2)
            {
                return;
            }
            if( (BoatLeft() && ((T > state[0] || R > state[1]) || (state[0] - T < state[1] - R && state[0] - T != 0) || (state[2] + T < state[3] + R) && state[2] + T != 0)) ||
                !BoatLeft() && ((T > state[2] || R > state[3]) || (state[2] - T < state[3] - R && state[2] - T != 0)|| (state[0] + T < state[1] + R) && state[0] + T != 0))
            {

                return;
            }
            int[] copy = CopyState();
            if (BoatLeft())
            {
                copy[0] -= T;
                copy[1] -= R;
                copy[2] += T;
                copy[3] += R;
            }
            else
            {
                copy[0] += T;
                copy[1] += R;
                copy[2] -= T;
                copy[3] -= R;
            }
            copy[4] = BoatLeft() ? 1 : 0;
            AddChild(copy);
        }
        #endregion


        public bool BoatLeft()
        {
            return state[4] == 0;
        }
        int[] CopyState()
        {
            int[] result = new int[state.Length];
            Buffer.BlockCopy(state, 0, result, 0, state.Length * sizeof(int));
            return result;
        }
        /// <summary>
        /// Prints the graph
        /// </summary>
        /// <param name="depth">Recursive parameter to check tree depth. Use 0</param>
        /// <param name="maxPrintDepth">Depth at which print cancels recursive calls</param>
        public void Print(int depth, int maxPrintDepth)
        {
            if (depth > maxPrintDepth)
            {
                return;
            }
            Console.WriteLine((new String('\t', depth)) + "(" + string.Join(",", state) + ")");
            foreach (NodeSecond s in children)
            {
                s.Print(depth + 1, maxPrintDepth);
            }
        }
    }
}
